(live-pack-name "helm-ag")
(live-pack-version "0.0.1alpha")
(live-pack-description "Contains Helm, async, and AG (SilverSearcher).")
