## User Pack Template

This is a template for your own user (or other purpose) pack.

git clone --recursive git@gitlab.com:chadhq/live-pack-helm-projectile-ag.git

### Helm
Helm MUST be [manually installed.] (https://github.com/emacs-helm/helm#quick-install-from-git
)

Edit the Makefile, and change the emacs alias to:


`/Applications/Emacs.app/Contents/MacOS/Emacs`

then run `make`

### AG (Silver Searcher)

brew install the_silver_searcher
