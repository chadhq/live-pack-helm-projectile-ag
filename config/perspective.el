(live-add-pack-lib "perspective")
(require 'perspective)
(persp-mode)

(live-add-pack-lib "persp-projectile")
(require 'persp-projectile)
